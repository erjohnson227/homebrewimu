// HAL.h

#ifndef _HAL_h
#define _HAL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "HALGyro.h"
#include "HALBaro.h"
#include "L3GD20_GYRO.h"
#include "BMP180_BARO.h"
#include "HAL_Namespace.h"

#define BUFFER_DEPTH	8

// We are using the Adafruit 10 DOF imu board. 
#define HAL_IMU_ADAFRUIT_10DOF		1

// Gyroscope types supported by the HAL layer. 
#define HAL_GYRO_L3GD20		0
#define HAL_GYRO_OTHER		1

// Barometer types supported by the HAL layer. 
#define HAL_BARO_BMP180		0
#define HAL_BARO_OTHER		1

// Define what hardware configuration we are using. 
#ifdef HAL_IMU_ADAFRUIT_10DOF

#define HAL_GYRO_TYPE	HAL_GYRO_L3GD20
#define HAL_BARO_TYPE	HAL_BARO_BMP180

#else

#define HAL_GYRO_TYPE	HAL_GYRO_OTHER
#define HAL_BARO_TYPE	HAL_GYRO_OTHER

#endif

class AP_HAL::HAL {

public:
	HAL(AP_HAL::HALBaro* _baro,
		AP_HAL::HalGyro* _gyro) 
		:
		barometer(_baro),
		gyroscope(_gyro)
	{}
			
	// Pointers to hold the static linked drivers. 
	HAL::HALBaro* barometer;
	HAL::HalGyro* gyroscope; 
	
};

#endif

