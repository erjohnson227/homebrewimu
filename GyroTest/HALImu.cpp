// 
// 
// 

#include "HALImu.h"


HALImu::HALImu()
{
	// Create a buffer. 
	if (_sampleBuf.init(BUFFER_DEPTH) < 0)
	{
		_error = true; 
	}

	// Create the L3GD20 object with desired initial parameters. 
	L3GD20 l3gd20(
		HalGyro::gyroDataRates::GYRO_RATE_100HZ,
		HalGyro::gyroRanges::GYRO_RANGE_250DPS,
		USE_FIFO,
		BUFFER_DEPTH,
		HalGyro::gyroIntTypes::GYRO_HARDWARE_INT);

	// Plug the gyro driver into the HAL. 
	_gyro = &l3gd20;
}

HALImu::HALImu(HalGyro * gyroDriver)
{
	// Plug the gyro driver into the HAL. 
	_gyro = gyroDriver; 
}

// Returns the pointer to the gyro object defined in the HAL. 
HalGyro* HALImu::getHalGyro(void)
{
	return _gyro;
}

int8_t HALImu::init(void)
{
	// Initialize the gyro with desired basic settings. 
	// Data rate and range are set through this function. 
	if (_gyro->init() < 0)
	{
		Serial.print("Error, initializing gyro.");
		return -1; 
	}

	// Now change the gyro to DYNAMIC STREAM mode and set the desired FIFO depth 
	// Also enable fifo threshold interrupt on INT2
	if(_gyro->config()<0)
	{
		Serial.print("Error, writing gyro FIFO settings.");
		return -1; 
	}

	return 0; 
}


