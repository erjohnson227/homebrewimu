/*
 Name:		GyroTest.ino
 Created:	9/7/2015 4:39:21 PM
 Author:	Evan Johnson
*/

// the setup function runs once when you press reset or power the board
#include "AP_HAL.h"
#include "HAL_Namespace.h"
#include "HAL_IMU_ADAFRUIT10DOF_Class.h"
#include "imu_adafruit_10Dof.h"
#include "BMP180_BARO.h"
#include "HAL.h"
#include "RingBuffer.h"
#include <Wire.h>
#include "L3GD20_GYRO.h"


// Flag for our simple round robin task scheduler to see when its time to read. 
bool gyroDataReady = false; 

void setup() {

	const AP_HAL::HAL& hal = IMU_HAL_IMU_ADAFRUIT10DOF; 


	// Open a serial connection for debugging purposes. 
	Serial.begin(9600); 


	if (hal_imu.init() < 0)
	{
		// Kill the program here on an error. Dont do anything else. 
		while (true);
	}

	cli(); 
	// Attach the data ready interrupt. 
	attachInterrupt(digitalPinToInterrupt(1), dataReady, RISING); 
	sei(); 
}

// the loop function runs over and over again until power down or reset
void loop() {

	// So far we wont do anything but check to see if data is available. 
	if (gyroDataReady)
	{
		// Avoid an interrup when we are in the middle of something important. 
		cli(); 
		hal_imu.update(); 
		sei(); 
		gyroDataReady = false; 
	}
}

void dataReady()
{
	// Update the gyro timestamp when new data becomes available. 
	gyro->lastRead = gyro->thisRead; 
	gyro->thisRead = micros(); 

	gyroDataReady = true; 
}
