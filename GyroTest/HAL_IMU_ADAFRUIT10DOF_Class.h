// HAL_IMU_ADAFRUIT10DOF_Class.h

#ifndef _HAL_IMU_ADAFRUIT10DOF_CLASS_h
#define _HAL_IMU_ADAFRUIT10DOF_CLASS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "HAL_Namespace.h"

class HAL_IMU_ADAFRUIT10DOF : public AP_HAL::HAL {
public: 
	HAL_IMU_ADAFRUIT10DOF(); 
	//void init(int argc, char * const argv[]) const; 
};

extern const HAL_IMU_ADAFRUIT10DOF IMU_HAL_IMU_ADAFRUIT10DOF; 


#endif

