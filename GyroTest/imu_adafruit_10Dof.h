// imu_adafruit_10Dof.h

#ifndef _IMU_ADAFRUIT_10DOF_h
#define _IMU_ADAFRUIT_10DOF_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "HALImu.h"

class AdafruitImu10Dof : public HALImu{

public: 
	// Default constructor
	AdafruitImu10Dof(void) {}; 


private:
	static L3GD20 _gyro; 
	static BMP180 _baro; 
};

#endif

