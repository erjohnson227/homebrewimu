// 
// 
// 

#include "L3GD20_GYRO.h"
#include "Arduino.h"

// We need Wire.h for I2C 
#include <Wire.h> 


void L3GD20::write8(l3gd20Registers_t reg, uint8_t value)
{
	Wire.beginTransmission((uint8_t)L3GD20_ADDRESS);
	Wire.write((uint8_t)reg);
	Wire.write((uint8_t)value);
	Wire.endTransmission();
}

uint8_t L3GD20::read8(l3gd20Registers_t reg)
{
	uint8_t value;

	//  Write the register we are interested in. 
	Wire.beginTransmission((uint8_t)L3GD20_ADDRESS);
	Wire.write((uint8_t)reg);
	Wire.endTransmission();

	//  Request and read one byte from the L3GD20
	Wire.requestFrom((uint8_t)L3GD20_ADDRESS, (uint8_t)1);
	while (!Wire.available());
	value = Wire.read();
	Wire.endTransmission();

	return value;
}

// -------------------- Top Level Functions --------------------------------
// ---------------- Called from above HAL Layer ----------------------------

int L3GD20::config(gyroDataRate_t dr, gyroRange_t rng, bool useFifo, uint8_t fifoDepth, gyroIntType_t intType)
{
	// Make sure to switch over from HAL data rates to L3GD20 Data Rates
	switch (dr)
	{
	case GYRO_RATE_12_5HZ:
		_dataRate = L3GD20_12_5HZ;
		break;
	case GYRO_RATE_25HZ:
		_dataRate = L3GD20_25HZ;
		break;
	case GYRO_RATE_50HZ:
		_dataRate = L3GD20_50HZ;
		break;
	case GYRO_RATE_100HZ:
		_dataRate = L3GD20_100HZ;
		break;
	case GYRO_RATE_200HZ:
		_dataRate = L3GD20_200HZ;
		break;
	case GYRO_RATE_400HZ:
		_dataRate = L3GD20_400HZ;
		break;
	case GYRO_RATE_800HZ:
		_dataRate = L3GD20_800HZ;
		break;
	default:
		//Not a data rate for the L3GD20, report an error. 
		return GYRO_ERROR_DATARATE_NOT_SUPPORTED;
	}

	switch (rng)
	{
	case GYRO_RANGE_250DPS:
		_range = L3GD20_RANGE_250DPS;
		break;
	case GYRO_RANGE_500DPS:
		_range = L3GD20_RANGE_500DPS;
		break;
	case GYRO_RANGE_2000DPS:
		_range = L3GD20_RANGE_2000DPS;
		break;
	default:
		//Not a valid range for the L3GD20, report an error. 
		return GYRO_ERROR_RANGE_NOT_SUPPORTED;

	}

	// Bring in the FIFO settings. 
	_fifoEnabled = useFifo;
	_fifoDepth = fifoDepth;

	// Start up the gyro in Bypass mode with the desired settings. 
	begin(_range, _dataRate);

	// Lets enable the FIFO. 
	if (_fifoEnabled && (_fifoDepth > 0))
	{
		enableFifo(_fifoEnabled, _fifoDepth);

		// Enable dynamic stream mode. Works best for burst reads. 
		setMode(L3GD20_DYNAMIC_STREAM_MODE);

		if (intType == GYRO_HARDWARE_INT)
		{
			// Set to interrupt on FIFO threshold. 
			configureInt2(true, INT2_FTH, false);
		}
	}

	return GYRO_INIT_SUCCESS;
}

int L3GD20::config(void)
{
	// Start up the gyro in Bypass mode with the desired settings. 
	begin(_range, _dataRate);

	// Lets enable the FIFO. 
	if (_fifoEnabled && (_fifoDepth > 0))
	{
		enableFifo(_fifoEnabled, _fifoDepth);

		// Enable dynamic stream mode. Works best for burst reads. 
		setMode(L3GD20_DYNAMIC_STREAM_MODE);

		if (_fifoInteruptType == GYRO_HARDWARE_INT)
		{
			// Set to interrupt on FIFO threshold. 
			configureInt2(true, INT2_FTH, false);
		}
	}


	return GYRO_INIT_SUCCESS;
}

int L3GD20::readGyro()
{
	// Check the FIFO level to see how many samples are ready to read. 
	uint8_t level = readFifoLevel();

	readRates(level); 
}


// -------------------------------------------------------------------------

/*
* Read out the desired number of samples from the gyro.
*/
void L3GD20::readRates(uint8_t numSamples)
{
	float degPerCount;

	switch (_range)
	{
	case L3GD20_RANGE_250DPS:
		degPerCount = L3GD20_SENSITIVITY_250DPS;
		break;
	case L3GD20_RANGE_500DPS:
		degPerCount = L3GD20_SENSITIVITY_500DPS;
		break;
	case L3GD20_RANGE_2000DPS:
		degPerCount = L3GD20_SENSITIVITY_2000DPS;
		break;
	}

	// Check if a new sample has occured since we received the watermark interrupt. 
	// If this is the case, then we will use the timestamp of when the watermark was 
	// reached but will assume that x additional timesteps have occured. 
	uint32_t deltaT = (thisRead - lastRead);

	if (numSamples > _fifoDepth)
	{
		// How many extra samples do we have?
		uint8_t extraSamples = numSamples - _fifoDepth;

		// Assume an equal time interval has occured and correct the deltaT accordingly for
		// the unanticipated samples. 
		deltaT += (deltaT * (extraSamples / _fifoDepth));
	}

	// Temporary storage for the 6 bytes. 
	uint8_t xlow, xhigh, ylow, yhigh, zlow, zhigh;

	// Read out the requested number of samples. 
	for (int sample = 0; sample < numSamples; sample++)
	{
		// Begin transmission to the gyroscope. 
		Wire.beginTransmission(L3GD20_ADDRESS);

		// Turn on auto increment. Set to read out data. 
		Wire.write(L3GD20_REGISTER_OUT_X_L | 0x80);
		Wire.endTransmission();

		//Request the 6 bytes of data. 
		Wire.requestFrom((uint8_t)L3GD20_ADDRESS, (uint8_t)6);

		while (Wire.available() < 6);

		// Read in 2-byte x value
		xlow = Wire.read();
		xhigh = Wire.read();

		// Read in 2-byte y value
		ylow = Wire.read();
		yhigh = Wire.read();

		// Read in 2-byte z value
		zlow = Wire.read();
		zhigh = Wire.read();

		// Read in the 16 bit values and convert to degrees per second. 
		fifoData.data[sample].xRate = (float)((xhigh << 8) | xlow) * degPerCount;
		fifoData.data[sample].yRate = (float)((yhigh << 8) | ylow) * degPerCount;
		fifoData.data[sample].zRate = (float)((zhigh << 8) | zlow) * degPerCount;

		// Timestamp the measurement. 
		fifoData.data[sample].timestamp = lastRead + (deltaT * (sample / numSamples)); 
	}

	// Set the fifo samples read. 
	fifoData.index = numSamples;
}
bool L3GD20::begin(l3gd20Range_t rng, l3gd20_data_rate_t odr)
{
	//  Keep track of selected range nad ODR. 
	_range = rng;
	_dataRate = odr;

	//  Default bandwidth. 
	_bandwidth = L3GD20_BW_HIGH;

	//  Starts in bypass mode. No FIFO enabled. 
	_fifoMode = L3GD20_FIFO_BYPASS_MODE;
	_fifoEnabled = false;

	//  Fifo depth default value of 0. 
	_fifoDepth = 0;



	// Start i2c and get the sensor up and running. 
	Wire.begin();

	uint8_t id = read8(L3GD20_REGISTER_WHO_AM_I);

	if ((id != L3GD20_ID) && (id != L3GD20H_ID))
	{
		return false;
	}

	//  Reset the power mode, turn everything off.
	write8(L3GD20_REGISTER_CTRL_REG1, 0x00);

	uint8_t ctrlreg = 0;

	//  Enable all three axes. 
	ctrlreg |= (1 << L3GD20_X_ENABLE) | (1 << L3GD20_Y_ENABLE) | (1 << L3GD20_Z_ENABLE);

	//  Exit sleep mode. 
	ctrlreg |= (1 << L3GD20_POWER_DWN);

	//  Select desired data rate. 
	//  Low speed ODR
	if (odr <= L3GD20_ODR_50_HZ)
	{
		ctrlreg |= (odr << L3GD20_ODR_SELECT);
	}
	else {  // Full speed ODR
		ctrlreg |= ((odr - 3) << L3GD20_ODR_SELECT);
	}

	//Write control register 1. 
	write8(L3GD20_REGISTER_CTRL_REG1, ctrlreg);

	ctrlreg = 0;

	ctrlreg |= (rng << L3GD20_FS);

	// Write conrtol register 4
	write8(L3GD20_REGISTER_CTRL_REG4, ctrlreg);


	//  Remember to enable low ODR mode. 
	if (odr <= L3GD20_ODR_50_HZ)
	{
		write8(L3GD20_REGISTER_LOW_ODR, (1 << L3GD20_LOW_ODR));
	}

	return true;
}

L3GD20::L3GD20(gyroDataRate_t dr, gyroRange_t rng, bool useFifo, uint8_t fifoDepth, gyroIntType_t intType = GYRO_SOFTWARE_INT)
{
	// Bring in settings from the constructor. 
	// Make sure to switch over from HAL data rates to L3GD20 Data Rates
	switch (dr)
	{
	case GYRO_RATE_12_5HZ:
		_dataRate = L3GD20_12_5HZ;
		break;
	case GYRO_RATE_25HZ:
		_dataRate = L3GD20_25HZ;
		break;
	case GYRO_RATE_50HZ:
		_dataRate = L3GD20_50HZ;
		break;
	case GYRO_RATE_100HZ:
		_dataRate = L3GD20_100HZ;
		break;
	case GYRO_RATE_200HZ:
		_dataRate = L3GD20_200HZ;
		break;
	case GYRO_RATE_400HZ:
		_dataRate = L3GD20_400HZ;
		break;
	case GYRO_RATE_800HZ:
		_dataRate = L3GD20_800HZ;
		break;
	default:
		break; 
	}

	switch (rng)
	{
	case GYRO_RANGE_250DPS:
		_range = L3GD20_RANGE_250DPS;
		break;
	case GYRO_RANGE_500DPS:
		_range = L3GD20_RANGE_500DPS;
		break;
	case GYRO_RANGE_2000DPS:
		_range = L3GD20_RANGE_2000DPS;
		break;
	}


	_fifoEnabled = useFifo; 
	_fifoDepth = fifoDepth;
	_fifoInteruptType = intType;
}

uint8_t L3GD20::init()
{
	// Start the gyro with the desired range and data rate settings. 
	begin(_range, _dataRate); 
}

void L3GD20::setOdr(l3gd20_data_rate_t odr)
{
	//Keep track of selected ODR.
	_dataRate = odr;

	uint8_t ctrlreg;

	// Read the contents of control register 1
	ctrlreg = read8(L3GD20_REGISTER_CTRL_REG1);

	//  Clear the data rate bits of the CTRL_REG1
	ctrlreg &= !(uint8_t)(0x03 << L3GD20_ODR_SELECT);

	//  Select desired data rate. 
	//  Low speed ODR
	if (odr <= L3GD20_ODR_50_HZ)
	{
		ctrlreg |= (odr << L3GD20_ODR_SELECT);
	}
	else {  // Full speed ODR
		ctrlreg |= ((odr - 3) << L3GD20_ODR_SELECT);
	}

	//Write control register 1. 
	write8(L3GD20_REGISTER_CTRL_REG1, ctrlreg);

	//  Remember to enable low ODR mode. 
	if (odr <= L3GD20_ODR_50_HZ)
	{
		write8(L3GD20_REGISTER_LOW_ODR, (1 << L3GD20_LOW_ODR));
	}
}

void L3GD20::setOdr(l3gd20_data_rate_t odr, l3gd20_bandwidth_t bw)
{
	//Keep track of selected ODR and bandwidth. 
	_dataRate = odr;
	_bandwidth = bw;

	uint8_t ctrlreg;

	// Read the contents of control register 1
	ctrlreg = read8(L3GD20_REGISTER_CTRL_REG1);

	//  Clear the data rate bits of the CTRL_REG1
	ctrlreg &= !(uint8_t)(0x03 << L3GD20_ODR_SELECT);

	//  Select desired data rate. 
	//  Low speed ODR
	if (odr <= L3GD20_ODR_50_HZ)
	{
		ctrlreg |= (odr << L3GD20_ODR_SELECT);
	}
	else {  // Full speed ODR
		ctrlreg |= ((odr - 3) << L3GD20_ODR_SELECT);
	}

	//  Select the bandwidth. 
	//  Clear the 2 bandwidth select bits. 
	ctrlreg &= (0x03 << L3GD20_BDW_SELECT);

	//  Set the bandwidth. 
	ctrlreg |= (bw << L3GD20_BDW_SELECT);

	//	Write control register 1. 
	write8(L3GD20_REGISTER_CTRL_REG1, ctrlreg);

	//  Read the Low_ODR Register
	ctrlreg = read8(L3GD20_REGISTER_LOW_ODR);

	//  Remember to enable low ODR mode. 
	if (odr <= L3GD20_ODR_50_HZ)
	{
		//  Set the low ODR control bit. 
		ctrlreg |= (1 << L3GD20_LOW_ODR);
	}
	else {
		//  Clear the LOW ODR control bit. 
		ctrlreg &= !(uint8_t)(1 << L3GD20_LOW_ODR);
	}

	//  Write the low ODR register to save changes 
	write8(L3GD20_REGISTER_LOW_ODR, ctrlreg);
}

void L3GD20::configureInt2(bool enable, l3gd20_int2_type type, bool readExisting)
{
	uint8_t ctrlreg = 0;

	if (readExisting)
	{
		//Read the existing contents of the control register. 
		ctrlreg = read8(L3GD20_REGISTER_CTRL_REG3);
	}

	if (enable)
	{
		// Mask out the existing interrupt bits. 
		ctrlreg &= !(uint8_t)(0b1111 < L3GD20_INT2_EMPTY);
		// Set the bit to enable the requested interrupt. 
		ctrlreg |= (1 << type);
		write8(L3GD20_REGISTER_CTRL_REG3, ctrlreg);
	}
	else {
		ctrlreg &= 0x0F; //Clear the lower four bits. Dont affect INT 1 
		write8(L3GD20_REGISTER_CTRL_REG3, ctrlreg);
	}
}

/**
* Sets the fifo mode. Will disable the FIFO buffer if Bypass mode is selected.
*/
void L3GD20::setMode(l3gd20FifoModes_t mode)
{
	// Keep track of the current FIFO mode. 
	if (mode > L3GD20_FIFO_BYPASS_MODE)
		_fifoEnabled = true;

	_fifoMode = mode;

	uint8_t ctrlreg;

	if (mode)  // Only if not in bypass mode. 
	{
		// Enable the FIFO buffer. 
		ctrlreg = read8(L3GD20_REGISTER_CTRL_REG5);
		ctrlreg |= (1 << L3GD20_FIFO_EN);
		write8(L3GD20_REGISTER_CTRL_REG5, ctrlreg);
	}
	else {
		//Bypass mode, disable the FIFO buffer. 
		ctrlreg = read8(L3GD20_REGISTER_CTRL_REG5);
		ctrlreg &= !(uint8_t)(1 << L3GD20_FIFO_EN);
		write8(L3GD20_REGISTER_CTRL_REG5, ctrlreg);

	}

	// Read the FIFO control register and set the new mode. 
	ctrlreg = read8(L3GD20_REGISTER_FIFO_CTRL_REG);
	ctrlreg |= (mode << L3GD20_FM);
	write8(L3GD20_REGISTER_FIFO_CTRL_REG, ctrlreg);
}

bool L3GD20::setFifoDepth(uint8_t depth, bool stopOnThreshold)
{

	// Keep track of FIFO depth and check if Stop on Threshold is enabled. 
	_fifoDepth = depth;
	_fifoStopOnThreshold = stopOnThreshold;

	uint8_t ctrlreg;

	ctrlreg = read8(L3GD20_REGISTER_FIFO_CTRL_REG);

	ctrlreg &= !(0b00011111);  //Clear the FIFO depth select bits.
							   // Set the FIFO threshold depth. 
	ctrlreg |= ((depth << L3GD20_FTH) & 0b00011111);
	write8(L3GD20_REGISTER_FIFO_CTRL_REG, ctrlreg);

	if (stopOnThreshold)
	{
		//set the stop on threshold bit. 
		ctrlreg = read8(L3GD20_REGISTER_CTRL_REG5);
		ctrlreg |= (1 << L3GD20_STOP_ON_FTH);
	}
	else {
		//Clear the stop on threshold bit. 
		ctrlreg = read8(L3GD20_REGISTER_CTRL_REG5);
		ctrlreg &= !(uint8_t)(1 << L3GD20_STOP_ON_FTH);
	}

	// Write control register 5 to apply changes. 
	write8(L3GD20_REGISTER_CTRL_REG5, ctrlreg);

	return true; //  No error checking for now.  
}

void L3GD20::enableFifo(bool enable)
{
	// Keep track of FIFO enable status
	_fifoEnabled = enable;

	uint8_t ctrlreg;

	ctrlreg = read8(L3GD20_REGISTER_CTRL_REG5);

	if (enable)
	{
		//Set the FIFO enable bit. 
		ctrlreg |= (1 << L3GD20_FIFO_EN);
	}
	else {
		//Clear the FIFO enable bit. 
		ctrlreg &= !(uint8_t)(1 << L3GD20_FIFO_EN);
	}

	write8(L3GD20_REGISTER_CTRL_REG5, ctrlreg);
}

void L3GD20::enableFifo(bool enable, uint8_t depth)
{
	// Keep track of FIFO enable status and depth. 
	_fifoEnabled = enable;
	_fifoDepth = depth;

	uint8_t ctrlreg;

	ctrlreg = read8(L3GD20_REGISTER_CTRL_REG5);

	// Enable or disable the FIFO buffer.
	if (enable)
	{
		//Set the FIFO enable bit. 
		ctrlreg |= (1 << L3GD20_FIFO_EN);
	}
	else {
		//Clear the FIFO enable bit. 
		ctrlreg &= !(uint8_t)(1 << L3GD20_FIFO_EN);
	}

	write8(L3GD20_REGISTER_CTRL_REG5, ctrlreg);


	//Set the FIFO depth. 
	ctrlreg = read8(L3GD20_REGISTER_FIFO_CTRL_REG);

	ctrlreg &= !(0b00011111);  //Clear the FIFO depth select bits. 
							   //Set the new FIFO depth. 
	ctrlreg |= ((depth << L3GD20_FTH) & 0b00011111);

	//Write the changes to the register. 
	write8(L3GD20_REGISTER_FIFO_CTRL_REG, ctrlreg);
}

uint8_t L3GD20::readFifoSrcReg()
{
	return read8(L3GD20_REGISTER_FIFO_SRC_REG);
}

bool L3GD20::checkFifoEmpty()
{
	uint8_t reg = read8(L3GD20_REGISTER_FIFO_SRC_REG);
	if (reg & (1 << L3GD20_FIFO_EMPTY))
	{
		return true;
	}

	return false;
}

bool L3GD20::checkFifoEmpty(uint8_t fifoSrcData)
{
	if (fifoSrcData & (1 << L3GD20_FIFO_EMPTY))
	{
		return true;
	}

	return false;
}

bool L3GD20::checkFifoOvrn()
{
	uint8_t reg = read8(L3GD20_REGISTER_FIFO_SRC_REG);
	if (reg & (1 << L3GD20_FIFO_OVRN))
	{
		return true;
	}

	return false;

}

bool L3GD20::checkFifoOvrn(uint8_t fifoSrcData)
{
	if (fifoSrcData & (1 << L3GD20_FIFO_OVRN))
	{
		return true;
	}

	return false;
}

bool L3GD20::checkFifoFth()
{
	uint8_t reg = read8(L3GD20_REGISTER_FIFO_SRC_REG);
	if (reg & (1 << L3GD20_FTH_STATUS))
	{
		return true;
	}

	return false;
}

bool L3GD20::checkFifoFth(uint8_t fifoSrcData)
{
	if (fifoSrcData & (1 << L3GD20_FTH_STATUS))
	{
		return true;
	}

	return false;
}

uint8_t L3GD20::readFifoLevel()
{
	uint8_t reg = read8(L3GD20_REGISTER_FIFO_SRC_REG);

	// Mask off the three flag bits. 
	return (reg & 0x1F);
}

uint8_t L3GD20::readFifoLevel(uint8_t fifoSrcData)
{
	// Mask off the three flag bits. 
	return (fifoSrcData & 0x1F);
}




