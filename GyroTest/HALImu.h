// imu.h

#ifndef _IMU_h
#define _IMU_h

#define BUFFER_DEPTH	8

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

//Include the ring buffer. 
#include "RingBuffer.h"
#include "HalGyro.h"
#include "L3GD20_GYRO.h"

class HAL_IMU
{

public: 
	// Default constructor. 
	HAL_IMU();
	HAL_IMU(HalGyro *, HALBaro *);

	// Initialization function. 
	int8_t init(void); 

	// Update the IMU 
	int8_t update(); 

	void dataReady(void); 

	//HalGyro * getHalGyro(void); 

protected:
	RingBuffer _sampleBuf;

private: 
	HalGyro * _gyro;
	HALBaro * _baro; 
	bool	_error;

};

#endif

