#ifndef __HALGYRO_H__
#define __HALGYRO_H__

#define USE_FIFO	1
#define NO_FIFO		0

#include "HAL_Namespace.h"

class AP_HAL::HalGyro {

	public: 
		HalGyro(); 

		// Possible error codes for the Gyro 
		enum gyroErrCodes {
			GYRO_ERROR_RANGE_NOT_SUPPORTED		= -2,
			GYRO_ERROR_DATARATE_NOT_SUPPORTED	= -1,

			// Return 0 for success
			GYRO_INIT_SUCCESS					=  0
		};

		typedef enum gyroIntTypes{
			GYRO_HARDWARE_INT,
			GYRO_SOFTWARE_INT
		} gyroIntType_t;

		//Data rate enum, contains all of the data rates possible in order. 
		typedef enum gyroDataRates{
			GYRO_RATE_12_5HZ,
			GYRO_RATE_25HZ,
			GYRO_RATE_50HZ,
			GYRO_RATE_100HZ,
			GYRO_RATE_200HZ,
			GYRO_RATE_400HZ,
			GYRO_RATE_800HZ
		}gyroDataRate_t;

		//Gyroscope range enum, contains all gyro ranges possible in order. 
		typedef enum gyroRanges{
			GYRO_RANGE_250DPS,
			GYRO_RANGE_500DPS,
			GYRO_RANGE_2000DPS
		} gyroRange_t;

		// Struct to hold gyro rates in deg/s along with timestamp of measurement. 
		typedef struct gyroData_s {
			float xRate;
			float yRate;
			float zRate;
			uint32_t timestamp;
		} gyroData;

		// 
		typedef struct gyroFifoData_s {
			gyroData data[32];
			uint8_t index;
		} gyroFifoData;

		//Virtual funcitons to be implemented in driver files. 

		virtual void read();

		virtual int config(gyroDataRate_t, gyroRange_t, bool, uint8_t, gyroIntType_t);

		virtual int config(void); 

		virtual uint8_t init(void); 

		// Public variables
		static gyroData data; 

		static gyroFifoData fifoData; 

		volatile static uint32_t lastRead; 
		volatile static uint32_t thisRead; 

protected: 
	gyroIntType_t _fifoInteruptType; 
};
#endif // !__HALGYRO_H__

