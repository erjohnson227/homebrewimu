// HAL_baro.h

#ifndef _HALBARO_h
#define _HALBARO_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "HAL_Namespace.h"

// Abstract interface class used to abstract implementation of BARO sensor. 
class AP_HAL::HALBaro {
	public:
		// 
		virtual bool begin(uint8_t) = 0; 
		virtual void getTemperature(float*) = 0;
		virtual void getPressure(float*) = 0; 
		virtual float pressureToAltitude(float, float) = 0; 
};
#endif

