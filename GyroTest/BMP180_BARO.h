// BMP180_BARO.h

#ifndef _BMP180_BARO_h
#define _BMP180_BARO_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "HALBaro.h"
#include "HAL_Namespace.h"

#define BMP085_USE_DATASHEET_VALS	false
#define BMP180_ADDRESS				(0x77)

typedef struct
{
	int16_t		ac1; 
	int16_t		ac2; 
	int16_t		ac3; 
	uint16_t	ac4; 
	uint16_t	ac5; 
	uint16_t	ac6; 
	int16_t		b1; 
	int16_t		b2; 
	int16_t		mb; 
	int16_t		mc; 
	int16_t		md;
} bmp180_calib_data;

class BMP180 : public AP_HAL::HALBaro{
public:
	// BMP180 Register map. 
	typedef enum bmp180registers {
		BMP180_REGISTER_CAL_AC1 = 0xAA, 
		BMP180_REGISTER_CAL_AC2 = 0xAC, 
		BMP180_REGISTER_CAL_AC3 = 0xAE, 
		BMP180_REGISTER_CAL_AC4 = 0xB0, 
		BMP180_REGISTER_CAL_AC5 = 0xB2, 
		BMP180_REGISTER_CAL_AC6 = 0xB4, 
		BMP180_REGISTER_CAL_B1 = 0xB6, 
		BMP180_REGISTER_CAL_B2 = 0xB8, 
		BMP180_REGISTER_CAL_MB = 0xBA, 
		BMP180_REGISTER_CAL_MC = 0xBC, 
		BMP180_REGISTER_CAL_MD = 0xBE, 
		BMP180_REGISTER_WHO_AM_I = 0xD0,
		BMP180_REGISTER_OUT_XLSB = 0xF8,
		BMP180_REGISTER_OUT_LSB = 0xF7,
		BMP180_REGISTER_OUT_MSB = 0xF6,
		BMP180_REGISTER_TEMPDATA = 0xF6,
		BMP180_REGISTER_PRESSUREDATA = 0xF6,
		BMP180_REGISTER_VERSION = 0xD1,
		BMP180_REGISTER_CTRL_MEAS = 0xF4,
		BMP180_REGISTER_SOFT_RST = 0xE0,
		BMP180_REGISTER_READTEMPCMD = 0x2E,
		BMP180_REGISTER_READPRESSURECMD = 0x34,
	} bmp180registers_t;

	typedef enum {
		BMP180_MODE_ULTRALOWPOWER = 0x00,
		BMP180_MODE_STANDARD = 0x01,
		BMP180_MODE_HIGHRES = 0x02,
		BMP180_MODE_ULTRAHIGHRES = 0x03
	} bmp180_mode_t;

	enum bmp180_register_out_xlsb {
		BMP180_ADC_OUT_XLSB = 0x03
	};

	enum bmp180_register_ctrl_meas {
		BMP180_MEAS_CTRL = 0x00,
		BMP180_SCO = 0x05,
		BMP180_OSS = 0x06
	};

	enum bmp180_ovrsmpl_sel {
		BMP180_OVRSMP_1X = 0x00,
		BMP180_OVRSMP_2X = 0x01,
		BMP180_OVRSMP_4X = 0x02,
		BMP180_OVRSMP_8X = 0x03
	};

	BMP180(void); 

	bool begin(uint8_t mode = BMP180_MODE_ULTRAHIGHRES);

	void getTemperature(float *temp);
	void getPressure(float *pressure);

	float pressureToAltitude(float, float); 
    //void readRawPressure(int32_t*);


private:
	int32_t computeB5(int32_t); 

	//uint8_t		_bmp180Mode; 
};

#endif

