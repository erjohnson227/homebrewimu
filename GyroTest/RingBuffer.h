// RingBuffer.h

#ifndef _RINGBUFFER_h
#define _RINGBUFFER_h

#include "HALGyro.h"

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class RingBuffer {
public:

	typedef struct sensorData_s {
		float xValue;
		float yValue;
		float zValue;
		uint32_t timestamp;
	} sensorData;

	typedef enum bufferErrors {
		BUFFER_ERR_OTHER = -3,
		BUFFER_ERR_OVRN = -2,
		BUFFER_ERR_SIZE = -1,
		BUFFER_INIT_SUCCESS = 0,
		BUFFER_DATA_SUCCESS = 0
	} bufferErrors_t;


	RingBuffer(uint8_t size);
	RingBuffer(void); 

	// Pop the current reading out of the buffer. 
	void * pop(void); 

	// Push a new sample into the buffer, returns the current buffer depth. 
	uint8_t push(sensorData sample); 

	// Clear the contents of the buffer, 
	bufferErrors_t init(void); 
	bufferErrors_t init(uint8_t); 


private:
	// Array to hold the buffer contents.
	sensorData * _data;

	uint8_t _index; 
	uint8_t _currentDepth; 
	uint8_t _bufferDepth; 


};

#endif

