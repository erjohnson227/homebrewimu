// HAL_Namespace.h

#ifndef _HAL_NAMESPACE_h
#define _HAL_NAMESPACE_h

#include "AP_HAL.h"

namespace AP_HAL {
	
	// Top level HAL class. 
	class HAL; 

	// Lower level HAL classes. 
	class HAL_IMU; 
	class HALBaro; 
	class HalGyro; 
};


#endif

