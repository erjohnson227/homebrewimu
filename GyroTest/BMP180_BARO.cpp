// 
// 
// 

#include "BMP180_BARO.h"
#include <Wire.h>


static uint8_t _bmp180Mode; 
static bmp180_calib_data _bmp180_coeffs; 

static void writeCommand(uint8_t reg, uint8_t value)
{
	Wire.beginTransmission((uint8_t)BMP180_ADDRESS);

	Wire.write(reg); 
	Wire.write(value); 

	Wire.endTransmission(); 
}

static void read8(uint8_t reg, uint8_t *value)
{
	Wire.beginTransmission((uint8_t)BMP180_ADDRESS); 

	Wire.write(reg);
	Wire.endTransmission(); 

	Wire.requestFrom((uint8_t)BMP180_ADDRESS, (uint8_t)1);

	*value = Wire.receive(); 

	Wire.endTransmission(); 
}

static void read16(uint8_t reg, uint16_t *value)
{
	Wire.beginTransmission((uint8_t)BMP180_ADDRESS);

	Wire.write(reg); 

	Wire.endTransmission(); 

	Wire.requestFrom((uint8_t)BMP180_ADDRESS, (uint8_t)2); 

	*value = (Wire.read() << 8) | Wire.read(); 

	Wire.endTransmission(); 
}

static void readS16(uint8_t reg, int16_t *value)
{
	uint16_t i; 
	read16(reg, &i); 
	*value = (int16_t)i; 
}

static void readCoefficients(void)
{
#if BMP085_USE_DATASHEET_VALS
	_bmp180_coeffs.ac1		= 408; 
	_bmp180_coeffs.ac2		= -72;
	_bmp180_coeffs.ac3		= -14383;
	_bmp180_coeffs.ac4		= 32741;
	_bmp180_coeffs.ac5		= 32757;
	_bmp180_coeffs.ac6		= 23153;
	_bmp180_coeffs.b1		= 6190;
	_bmp180_coeffs.b2		= 4; 
	_bmp180_coeffs.mb		= -32768; 
	_bmp180_coeffs.mc		= -8711; 
	_bmp180_coeffs.md		= 2868;
	_bmp180Mode				= 0; 
#else
	readS16(BMP180::BMP180_REGISTER_CAL_AC1, &_bmp180_coeffs.ac1);
	readS16(BMP180::BMP180_REGISTER_CAL_AC2, &_bmp180_coeffs.ac2);
	readS16(BMP180::BMP180_REGISTER_CAL_AC3, &_bmp180_coeffs.ac3);
	read16(BMP180::BMP180_REGISTER_CAL_AC4, &_bmp180_coeffs.ac4);
	read16(BMP180::BMP180_REGISTER_CAL_AC5, &_bmp180_coeffs.ac5);
	read16(BMP180::BMP180_REGISTER_CAL_AC6, &_bmp180_coeffs.ac6);
	readS16(BMP180::BMP180_REGISTER_CAL_B1, &_bmp180_coeffs.b1);
	readS16(BMP180::BMP180_REGISTER_CAL_B2, &_bmp180_coeffs.b2);
	readS16(BMP180::BMP180_REGISTER_CAL_MB, &_bmp180_coeffs.mb);
	readS16(BMP180::BMP180_REGISTER_CAL_MC, &_bmp180_coeffs.mc);
	readS16(BMP180::BMP180_REGISTER_CAL_MD, &_bmp180_coeffs.md);
#endif
}

static void readRawPressure(int32_t * pressure)
{
	uint8_t p8;
	uint16_t p16;
	int32_t p32;

	writeCommand(
		BMP180::BMP180_REGISTER_CTRL_MEAS,
		BMP180::BMP180_REGISTER_READPRESSURECMD + (_bmp180Mode << BMP180::BMP180_OSS));

	// Wait for the conversion to complete. 
	switch (_bmp180Mode)
	{
	case BMP180::BMP180_MODE_ULTRALOWPOWER:
		delay(5);
		break;
	case BMP180::BMP180_MODE_STANDARD:
		delay(8);
		break;
	case BMP180::BMP180_MODE_HIGHRES:
		delay(14);
		break;
	case BMP180::BMP180_MODE_ULTRAHIGHRES:
		delay(26);
		break;
	}

	// Read out the entire 
	read16(BMP180::BMP180_REGISTER_PRESSUREDATA, &p16);
	p32 = (uint32_t)p16 << 8; 
	read8(BMP180::BMP180_REGISTER_PRESSUREDATA + 2, &p8); 
	p32 += p8; 
	p32 >>= (8 - _bmp180Mode);

	// Modify the result. 
	*pressure = p32; 
}

static void readRawTemperature(int32_t *temperature)
{
	uint16_t t; 
	writeCommand(BMP180::BMP180_REGISTER_CTRL_MEAS, BMP180::BMP180_REGISTER_READTEMPCMD);
	delay(5); 
	read16(BMP180::BMP180_REGISTER_TEMPDATA, &t); 
	*temperature = t; 
}

int32_t BMP180::computeB5(int32_t ut)
{
	int32_t X1 = (ut - (int32_t)_bmp180_coeffs.ac6) * ((int32_t)_bmp180_coeffs.ac5) >> 15; 
	int32_t X2 = ((int32_t)_bmp180_coeffs.mc << 11) / (X1 + (int32_t)_bmp180_coeffs.md); 

	return X1 + X2; 
}

// Default constructor. 
BMP180::BMP180(void)
{
	// Nothing to do here for now. 
}

bool BMP180::begin(uint8_t mode)
{
	// Open an I2C connection if not already opened. 
	Wire.begin();

	if ((mode > BMP180_MODE_ULTRAHIGHRES) || (mode < 0))
	{
		mode = BMP180_MODE_ULTRAHIGHRES; 
	}

	// Check to make sure that we have the right sensor on the other end. 
	uint8_t id; 
	read8(BMP180_REGISTER_WHO_AM_I, &id); 

	// Cant communicate with the sensor. 
	if (id != 0x55)
	{
		return false; 
	}
	readCoefficients(); 

	return true; 
}

void BMP180::getPressure(float *pressure)
{
	int32_t ut = 0, up = 0, compp = 0; 
	int32_t x1, x2, b5, b6, x3, b3, p; 
	uint32_t b4, b7; 

	// Get raw temperature and pressure values. 
	readRawTemperature(&ut); 
	readRawPressure(&up); 


	b5 = computeB5(ut); 

	b6 = b5 - 4000; 
	x1 = (_bmp180_coeffs.b2 * ((b6*b6) >> 12)) >> 11; 
	x2 = (_bmp180_coeffs.ac2 * b6) >> 11; 
	x3 = x1 + x2; 
	b3 = (((((int32_t)_bmp180_coeffs.ac1) * 4 + x3) << _bmp180Mode) + 2) >> 2; 
	x1 = (_bmp180_coeffs.ac3 * b6) >> 13; 
	x2 = (_bmp180_coeffs.b1 * ((b6 * b6) >> 12)) >> 16; 
	x3 = ((x1 + x2) + 2) >> 2; 
	b4 = (_bmp180_coeffs.ac4 * (uint32_t)(x3 + 32768)) >> 15; 
	b7 = ((uint32_t)(up - b3)*(50000 >> _bmp180Mode));

	if (b7 < 0x80000000)
	{
		p = (b7 << 1) / b4; 
	}
	else
	{
		p = (b7 / b4) << 1; 
	}

	x1 = (p >> 8) * (p >> 8); 
	x1 = (x1 * 3038) >> 16; 
	x2 = (-7357 * p) >> 16; 
	compp = p + ((x1 + x2 + 3791) >> 4); 

	//Assign compensated pressure value. 
	*pressure = compp; 
}

float BMP180::pressureToAltitude(float seaLevel, float atmospheric)
{
	return 44330.0 * (1.0 - pow(atmospheric / seaLevel, 0.1903)); 
}