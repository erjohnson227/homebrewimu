// 
// 
// 

#include "RingBuffer.h"



// Constructor for RingBuffer class. 
RingBuffer::RingBuffer(uint8_t numSamples)
{
	// Current sample depth is 0. 
	_currentDepth = 0; 

	// Next sample will be stored at position zero. 
	_index = 0; 

	// Buffer depth is equal to the number of samples. 
	_bufferDepth = numSamples; 

	//Allocate the buffer to hold the measurements. 
	_data = (sensorData*)malloc(numSamples*sizeof(sensorData)); 

	// Set the current index to zero. 
	_index = 0; 
}

RingBuffer::RingBuffer(void)
{
	//Default constructor
}

// Initialization of the buffer. 
RingBuffer::bufferErrors_t RingBuffer::init(void)
{
	if (_bufferDepth > 32)
	{
		return BUFFER_ERR_SIZE;
	}

	// Initialize the buffer contents to zero. 
	memset(_data, 0x00, _bufferDepth*sizeof(sensorData)); 

	return BUFFER_INIT_SUCCESS;
}

// Initialization of the buffer. 
RingBuffer::bufferErrors_t RingBuffer::init(uint8_t numSamples)
{
	// Current sample depth is 0. 
	_currentDepth = 0;

	// Next sample will be stored at position zero. 
	_index = 0;

	// Buffer depth is equal to the number of samples. 
	_bufferDepth = numSamples;

	if (_bufferDepth > 32)
	{
		return BUFFER_ERR_SIZE;
	}

	//Allocate the buffer to hold the measurements. 
	_data = (sensorData*)malloc(numSamples*sizeof(sensorData));

	// Set the current index to zero. 
	_index = 0;

	// Initialize the buffer contents to zero. 
	memset(_data, 0x00, _bufferDepth*sizeof(sensorData));

	return BUFFER_INIT_SUCCESS;
}

uint8_t RingBuffer::push(sensorData sample)
{
	bool overrun = false; 

	// Check for ring buffer overrun. 
	if (_currentDepth >= _bufferDepth)
	{
		overrun = true;
		_currentDepth = _bufferDepth;
	}
	else {
		_currentDepth++; 
	}

	// Copy the sample into the buffer. 
	_data[_index] = sample; 

	// Increment the index, and wrap around the buffer. 
	_index = (_index + 1) % _bufferDepth; 

	if (overrun)
	{
		return BUFFER_ERR_OVRN;
	}

	return BUFFER_DATA_SUCCESS; 
}

void * RingBuffer::pop(void)
{
	if (_currentDepth >= _bufferDepth)
	{
		_currentDepth = _bufferDepth; 

		// Buffer has been overflowed. 
		return NULL; 
	}
	if (_currentDepth == 0)
	{
		// Buffer is empty. 
		return NULL; 
	}
	int8_t dataIndex = _index - _currentDepth; 

	// Make sure the data index is a positive integer. 
	if (dataIndex < 0)
		dataIndex += _bufferDepth; 

	// Decrease the current depth of the buffer. 
	_currentDepth--; 

	// Return a pointer to the current data being read out. 
	return (void *)&_data[dataIndex];
}