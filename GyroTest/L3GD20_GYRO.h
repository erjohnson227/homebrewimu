// L3GD20_GYRO.h

#ifndef _L3GD20_GYRO_h
#define _L3GD20_GYRO_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

//  Include the HAL defines and abstraction class. 
#include "HALGyro.h"
#include "HAL_Namespace.h"


#define L3GD20_ADDRESS	0x6B
#define L3GD20_ID 		0xD4
#define L3GD20H_ID		0xD7

// Sensitivity of gyro for each range setting in degrees per count. 
#define L3GD20_SENSITIVITY_250DPS	(0.00875F) 
#define L3GD20_SENSITIVITY_500DPS	(0.0175F)
#define L3GD20_SENSITIVITY_2000DPS	(0.070F)

class L3GD20 : public AP_HAL::HalGyro {

public:

	//  L3GD20 Register map 
	typedef enum
	{	//Register Name					     Address			Default			Type
		L3GD20_REGISTER_WHO_AM_I				= 0X0F, 		//  0b11010111	 	r 
		L3GD20_REGISTER_CTRL_REG1			= 0X20, 		//  0b00000111	 	rw
		L3GD20_REGISTER_CTRL_REG2			= 0X21, 		//  0b00000000		rw
		L3GD20_REGISTER_CTRL_REG3			= 0X22, 		//  0b00000000		rw
		L3GD20_REGISTER_CTRL_REG4			= 0X23, 		//  0b00000000		rw
		L3GD20_REGISTER_CTRL_REG5			= 0X24, 		//  0b00000000		rw
		L3GD20_REGISTER_REFERENCE			= 0X25,		//  0b00000000		rw
		L3GD20_REGISTER_OUT_TEMP				= 0X26, 	 	//  --output--		r
		L3GD20_REGISTER_STATUS_REG			= 0X27, 		//  --output--		r
		L3GD20_REGISTER_OUT_X_L				= 0X28, 		//  --output--		r
		L3GD20_REGISTER_OUT_X_H				= 0X29, 		//  --output--		r
		L3GD20_REGISTER_OUT_Y_L				= 0X2A, 		//  --output--		r
		L3GD20_REGISTER_OUT_Y_H				= 0X2B, 		//  --output--		r
		L3GD20_REGISTER_OUT_Z_L				= 0X2C, 		//  --output--		r
		L3GD20_REGISTER_OUT_Z_H				= 0X2D,		//  --output--		r
		L3GD20_REGISTER_FIFO_CTRL_REG		= 0X2E,		//  0b00000000		rw
		L3GD20_REGISTER_FIFO_SRC_REG			= 0X2F,		//  --output--		r
		L3GD20_REGISTER_INT1_CFG				= 0X30,		//  0b00000000		r
		L3GD20_REGISTER_INT1_SRC				= 0X31,		//  --output--		r
		L3GD20_RESISTER_TSH_XH				= 0X32, 		//  0b00000000		rw
		L3GD20_REGISTER_TSH_XL				= 0X33, 		//  0b00000000		rw
		L3GD20_REGISTER_TSH_YH				= 0X34,  	//  0b00000000		rw
		L3GD20_REGISTER_TSH_YL				= 0X35,  	//  0b00000000		rw
		L3GD20_REGISTER_TSH_ZH				= 0X36, 		//  0b00000000		rw 
		L3GD20_REGISTER_TSH_ZL				= 0X37, 		//  0b00000000		rw
		L3GD20_REGISTER_INT1_DURATION		= 0X38, 		//  0b00000000		rw 
		L3GD20_REGISTER_LOW_ODR				= 0X39 		//  0b00000000		rw
	} l3gd20Registers_t;

	//	Positions of settings within control register 1. 
	enum l3gd20_register_ctrl_reg1 {
		L3GD20_X_ENABLE = 0x00, 			//  X-axis enable bit. 
		L3GD20_Y_ENABLE = 0x01,			//  Y-axis enable bit. 
		L3GD20_Z_ENABLE = 0x02,			//  Z-axis enable bit. 
		L3GD20_POWER_DWN = 0x03,			// 	Power down bit, 0-sleep mode, 1 power on
		L3GD20_BDW_SELECT = 0x04,		//  Bandwidth selection bits (2 bits)
		L3GD20_ODR_SELECT = 0x06			//  ODR Select bits (2 bits)
	};

	enum l3gd20_register_ctrl_reg2 {
		L3GD20_HPCF = 0x00, 				//  High pass cutoff frequency select (4 bit). 
		L3GD20_HPM = 0x04, 				//  High pass filter mode select bits (2 bits)
		L3GD20_LVL_EN = 0x06,			//  Level sensitive trigger enable bit. 
		L3GD20_EXTR_EN = 0x07			//  Edge sensitive trigger enable bit. 
	};

	enum l3gd20_register_ctrl_reg3 {
		L3GD20_INT2_EMPTY = 0x00,			//  Enable FIFO empty interrupt on DRDY/INT_2 
		L3GD20_INT2_OVRN = 0x01,				//  Enable FIFO overrun interrupt on DRDY/INT_2
		L3GD20_INT2_FTH = 0x02, 				//  Enable FIFO threshold interrupt on DRDY/INT_2
		L3GD20_INT2_DRDY = 0x03, 			//  Enable data ready interrupt on DRDY/INT_2
		L3GD20_PP_OD = 0x04, 			    //  Push pull / open drain on interrupt pins (default 0: push pull/ 1: open drain)
		L3GD20_H_L_ACTIVE = 0x05,   			//  Interrupt active configuration. (default 0: high, 1: low)
		L3GD20_INT1_BOOT = 0x06, 			//  Boot status on INT1 pin. 
		L3GD20_INT1_EN = 0x07				//  Interrupt enable on INT1 pin. 
	};

	typedef enum {
		INT2_EMPTY,
		INT2_OVRN,
		INT2_FTH,
		INT2_DRDY
	} l3gd20_int2_type;

	enum l3gd20_register_ctrl_reg4 {
		L3GD20_SIM = 0x00, 				//  SPI Serial interface mode (not used) 
		LEGD20_ST_EN = 0x01,					//  Self-test enable bits (2 bits)
		L3GD20_IMP_EN = 0x03,					//  Level sensitive latch enable
		L3GD20_FSS = 0x04, 				//  Full scale selection register (select scale)
		L3GD20_BLE = 0x06, 				//  Big / little endian selection (0: LSB First  1: MSB First)
		L3GD20_BDU = 0x07					//  Block data update select (0: continuous  1: wait for MSB and LSB reading)
	};

	/*
	* Positions of settings within Control Register 5.
	*
	* Output Selection Configuration:
	* The source of the output data can be selected, this allows the data to be selected before or after HPF and LPF2
	*
	*  Value:  			Description:
	*  0b00				Output selected from after LPF1
	*  0b01 				Output selected from after HPF
	*  0b1X				Output selected from after LPF2
	*
	* Interrupt Generator Selection Configuration:
	* The source of the output data can be selected, this allows the data to be selected before or after HPF and LPF2
	*
	*  Value: 				Description:
	*  0b00				Interrupts generated from after LPF1
	*  0b01				Interrupts generated from after HPF
	*  0b1X			    Interrupts generated from after LPF2
	*/

	enum l3gd20_register_ctrl_reg5 {
		L3GD20_OUT_SEL = 0x00, 				// Output selection configuration. (2 bits)
		L3GD20_IG_SEL = 0x02, 				// INT generator selection configuration (2 bits)
		L3GD20_HP_EN = 0x04, 				// High pass flter enable bit. 
		L3GD20_STOP_ON_FTH = 0x05, 				// Stop on FIFO threshold (0: depth not limited 1: stop at threshold depth)
		L3GD20_FIFO_EN = 0x06, 				// FIFO enable bit (0: FIFO disabled, 1: FIFO enabled)
		L3GD20_BOOT = 0x07, 				// Reboot memeory contenst, write 1 to reset memory contents. 
	};

	enum l3gd20_register_status {
		L3GD20_XDA = 0x00, 				//  Data available on the X axis. 
		L3GD20_YDA = 0x01, 				//  Data available on the Y axis. 
		L3GD20_ZDA = 0x02, 				//  Data available on the Z axis.
		L3GD20_ZYXDA = 0x03, 				// 	Data available on all three axes. 
		L3GD20_XOR = 0x04, 				//  X axis data overrun. 
		L3GD20_YOR = 0x05, 				//  Y axis data overrun. 
		L3GD20_ZOR = 0x06, 				//  Z axis data overrun. 
		L3GD20_ZYXOR = 0x07					//  Overwrite occured of previous data set on all three axes. 
	};

	enum l3gd20_register_fifo_ctrl {
		L3GD20_FTH = 0x00,									//  FIFO threshold level selection.  
		L3GD20_FM = 0x05 									//  FIFO mode selection.
	};

	enum l3gd20_register_fifo_src {
		L3GD20_FS = 0x00,					// FIFO stored data level of unread samples. 
		L3GD20_FIFO_EMPTY = 0x05, 				// FIFO empty flag bit. 
		L3GD20_FIFO_OVRN = 0x06, 				// FIFO overrun bit, fifo is completely filled.
		L3GD20_FTH_STATUS = 0x07					// FIFO threshold status (1: FIFO >= FTH level, 0: FIFO < FTH level)
	};


	enum l3gd20_register_ig_cfg {
		L3GD20_XLIE = 0x00, 				//  Enable interrupt on X axis low level event. 
		L3GD20_XHIE = 0x01, 				//  Enable interrupt on X axis high level event. 
		L3GD20_YLIE = 0x02, 				//  Enable interrupt on Y axis low level event. 
		L3GD20_YHIE = 0x03,					//  Enable interrupt on Y axis high level event. 
		L3GD20_ZLIE = 0x04, 				//  Enable interrupt on Z axis low level event. 
		L3GD20_ZHIE = 0x05, 				//  Enable interrupt on Z axis high level event. 
		L3GD20_LIR = 0x06, 				//  Interrupt request latch enable (1: latch enabled, 0: latch disabled)
		L3GD20_AND_OR = 0x07					//  ANF/OR combination of interrupts (1: interrupt on AND, 0: interrupt on OR)
	};

	/*
	* Interrupt generation source register.
	* Readint this register will reset IG_SRC IA bit and will also eventually reset the interrupt
	* signal on INT1. This also allows a refresh of the data within IG_SRC if the latch option was chosen.
	*/
	enum l3gd20_register_ig_src {
		L3GD20_IG_XL = 0x00,					//  X low event has occured  
		L3GD20_IG_XH = 0x01,					//  X high event has occured
		L3GD20_IG_YL = 0x02, 				//  Y low event has occured 
		L3GD20_IG_YH = 0x03,  				//  Y high event has ocured 
		L3GD20_IG_ZL = 0x04,					//  Z low event has occured
		L3GD20_IG_ZH = 0x05, 				//  Z high event has occured
		L3GD20_IG_IA = 0x06 					//  Interrupt active, one or more interrupts have been triggered. 
	};


	// Threshold registers for level interrupts. 

	enum l3gd20_register_ig_ths_xh {
		L3GD20_THSX_H = 0x00,					//  Upper 7 bits of the X threshold selection 
		L3GD20_DCRM = 0x07	 				//  Interrupt counter mode (0: Reset  1: Decrement)
	};

	enum l3gd20_register_ig_ths_xl {
		L3GD20_THSX_L = 0x00  				//  Lower byte of the X threshold selection
	};

	enum l3gd20_register_ig_ths_yh
	{
		L3GD20_THSY_H = 0x00					//  Upper 7 bits of the Y threshold selection 
	};

	enum l3gd20_register_ig_ths_yl {
		L3GD20_THSY_L = 0x00  				//  Lower byte of the Y threshold selection
	};

	enum l3gd20_register_ig_ths_zh
	{
		L3GD20_THSZ_H = 0x00					//  Upper 7 bits of the Z threshold selection 
	};

	enum l3gd20_register_ig_ths_zl {
		L3GD20_THSZ_L = 0x00  				//  Lower byte of the Z threshold selection
	};

	/*
	* Duration selection for level interrupts.
	* When wait enable is set to 1, the interrupt will not be disabled until the same duration has passed without
	* the level criteria being set. If wait enable is set to 0 then the interrupt will be cleared the instant that a sample
	* is read below the threshold value.
	*/
	enum l3gd20_register_ig_duration {
		L3GD20_DURATION = 0x00,					//  Interrupt duration in counts of ODR period. 
		L3GD20_WAIT = 0x07, 				//  Wait enable.
	};

	enum l3gd20_register_low_odr {
		L3GD20_LOW_ODR = 0x00, 				//  Enable low speed ODR mode. 
		L3GD20_SW_RES = 0x02, 				//  Software reset, write 1 to reset the device. 
		L3GD20_I2C_DIS = 0x03, 				//  Disable the I2C Interface (wont use this)
		L3GD20_DRDY_HL = 0x05  				//  DRDY/INT2 pin active level (0: DRDY active high, 1 DRDY active low); 
	};

	/*
	* Fifo mode selection. Enables and selects the mode of operation for the 32x16bit fifo buffer
	* for each channel of the gyroscope. Contains 7 user-selectable modes.
	*
	* Mode 0: 		Fifo Disabled   bypass mode, no data is held in the FIFO buffer, and the data must
	* be read out immediately in order to prevent an overwrite from occuring. Bypass mode can also be
	* used to reset the contents of the FIFO buffer, then you could switch back to FIFO mode to begin
	* accumulating data again.
	*
	* Mode 1: 		FIFO Mode  		Standard fifo mode, the data will be written to the fifo which will
	* fill up the 32 sample FIFO buffer. The fifo buffer will then be overwritten as additional samples
	* are brought in. The FIFO buffer can be reduced in depth through the use of the FIFO_CTRL register
	* bits 4 through 0. The fifo depth is specified by FIFO_CTRL(FTH5:0) - 1. The L3GD20 can be set to
	* stop collecting data when specified depth is reached, this is set through the StopOnFTH bit, which
	* is within Control Register 5. In addition, a FIFO threshold interrupt can be set to occur on
	* interrupt pin 2. This iterrupt is INT2_ORun which occurs when the first sample of the FIFO buffer
	* has been overwritten. This can be enabled in Control Register 3 and will either occur when
	* overwrite occurs or when the FIFO buffer has been filled to the level specified within
	* FIFO_CTL(FTH5:0) - 1.
	*
	* Mode 2:		Stream Mode-Dynamic Stream  Stream mode provides  continuus FIFO update. As new data
	* flows in, old data will be overwritten. The older data is pushed out of the FIFO buffer. An overrun
	* interrupt can be enabled through the setting of CTRL3(INT2_Orun) = 1. This allows all of the data in
	* the FIFO buffer to be read at once. This is NOT good if the application requires that no data should
	* be lost. If the user must ensure that no data is lost, a FIFO threshold interrupt can be set in order
	* to read the partially filled FIFO freeing up memory slots for incomming data. This can be done by
	* setting the the FIFO_CTRL(FTH4:0) to N value, which will be the number of samples that should be read
	* when the FIFO has rissen to level N+1. In dynamic stream mode, the threshold shouls be set between 1
	* and 30 when active. In dynamic stream mode, the number of samples read is independent of the number of
	* samples within the FIFO buffer.

	* Mode 3: 		Stream to FIFO mode   In this mode, the FIFO acts in stream mode until IG_SRC(IA) is set
	* indicating an level threshold interrupt has been triggered. When this occurs, the mode switches to FIFO
	* mode in order to capture the region of interest. Just like normal FIFO mode, once the FIFO buffer is filled
	* and the first sample is overwritten, data collection stops. In this case, a latched interrupt should be used
	* in order to prevent the system from returning to stream mode before the data can be read.

	* Mode 4: 		Bypass to Stream mode  	In this mode, the system operates in bypass mode until IG_SRC(IA) has
	* been set. Once this occurs, the system switches to FIFO mode and fills the FIFO buffer. Once again, a latched
	* interrupt should be used.

	* Mode 5: 		Dynamic Stream mode
	* Mode 6: 		Bypass to FIFO mode   	Similar to mode 3 and mode 4. Operates in bypass mode until IG_SRC(IA)
	* is set, then switches to FIFO mode and fills the FIFO buffer.
	*/
	typedef enum
	{
		L3GD20_FIFO_BYPASS_MODE = 0x00, 		//FIFO Disabled, bypass mode
		L3GD20_FIFO_MODE = 0x01,			//FIFO mode, simple FIFO
		L3GD20_STREAM_MODE = 0x02, 		//Stream Mode
		L3GD20_STREAM_TO_FIFO_MODE = 0x03,			//Stream to FIFO mode. 
		L3GD20_BYPASS_TO_STREAM_MODE = 0x04,			//Bypass to stream mode. 
		L3GD20_DYNAMIC_STREAM_MODE = 0x06,			//Dynamic stream mode.
		L3GD20_BYPASS_TO_FIFO_MODE = 0x07			//Bypass to fifo mode. 
	} l3gd20FifoModes_t;

	/* Full ODR data rates typedef. Contains the ODR selection values for
	* the full ODR operating range.
	*/
	typedef enum {
		L3GD20_ODR_100HZ = 0x00,
		L3GD20_ODR_200HZ = 0x01,
		L3GD20_ODR_400HZ = 0x02,
		L3GD20_ODR_800HZ = 0x03
	} l3gd20_odr_t;

	/* Low ODR data rates typedef. Contains the ODR selection values for
	* the lower ODR operating range.
	*/
	typedef enum {
		L3GD20_ODR_12_5HZ = 0X00,
		L3GD20_ODR_25_HZ = 0x01,
		L3GD20_ODR_50_HZ = 0x02
	} l3gd20_low_odr_t;

	//Data rate enum, contains all of the data rates possible in order. 
	typedef enum {
		L3GD20_12_5HZ,
		L3GD20_25HZ,
		L3GD20_50HZ,
		L3GD20_100HZ,
		L3GD20_200HZ,
		L3GD20_400HZ,
		L3GD20_800HZ
	}l3gd20_data_rate_t;

	// Actual bandwidth depends on the ODR selected. 
	typedef enum {
		L3GD20_BW_VERYLOW,
		L3GD20_BW_LOW,
		L3GD20_BW_MED,
		L3GD20_BW_HIGH
	}l3gd20_bandwidth_t;

	typedef enum
	{
		L3GD20_RANGE_250DPS,
		L3GD20_RANGE_500DPS,
		L3GD20_RANGE_2000DPS
	} l3gd20Range_t;

	//  Funtions

	//  Default prototype 
	L3GD20(gyroDataRate_t dr, gyroRange_t rng, bool useFifo, uint8_t fifoDepth, gyroIntType_t intType = GYRO_SOFTWARE_INT);

	// ---------------------- HAL Functions -------------------------------
	// High level functions to be called from above HAL layer. 

	uint8_t init();

	int config(gyroDataRate_t dr, gyroRange_t rng, bool useFifo, uint8_t fifoDepth, gyroIntType_t intType = GYRO_SOFTWARE_INT);

	int config(void); 

	int readGyro();

	// -------------------------------------------------------------------

	/* L3GD20 Initialization funciton*/
	bool begin(l3gd20Range_t rng = L3GD20_RANGE_250DPS, l3gd20_data_rate_t odr = L3GD20_100HZ);

	/* ----------------- Gyroscope configuration functions ----------------- */

	// Set the output data rate of the gyro. 
	void setOdr(l3gd20_data_rate_t odr);

	// Set the output data rate of the gyro and bandwidth. 
	void setOdr(l3gd20_data_rate_t odr, l3gd20_bandwidth_t bw);

	// Enable the FIFO and optionally set a FIFO depth. 
	void enableFifo(bool enable);
	void enableFifo(bool enable, uint8_t depth);

	// Set the FIFO mode. Choosing any mode other than Bypass enables the FIFO
	void setMode(l3gd20FifoModes_t mode);

	// Seet the FIFO depth, and choose whether to stop sample collection after depth is reached. 
	bool setFifoDepth(uint8_t depth, bool stopOnThreshold);

	// Configure interrupt 2 source and enable
	void configureInt2(bool enable, l3gd20_int2_type type, bool readExisting = true);

	/* --------------------------------------------------------- */


	/* ------------- Gyroscope Status Funcitons ---------------- */
	//  Check if the FIFO is empty currently. 
	//  Note that reading any of these functions will reset the FIFO_SRC 
	//  register. 
	bool checkFifoEmpty();

	// Check if the FIFO has been overrun. 
	bool checkFifoOvrn();

	// Check if the FIFO has reached the threshold.
	bool checkFifoFth();

	// Read the current FIFO level. 
	uint8_t readFifoLevel();

	// Reads the full FIFO_SRC register for procressing. 
	uint8_t readFifoSrcReg();

	// Overloaded functions to take a FIFO_SRC register and pull data from it
	bool checkFifoEmpty(uint8_t fifoSrcData);
	bool checkFifoOvrn(uint8_t fifoSrcData);
	bool checkFifoFth(uint8_t fifoSrcData);
	uint8_t readFifoLevel(uint8_t fifoSrcData);

	// Read a certain number of rate samples from the fifo buffer. 
	void readRates(uint8_t numSamples);

	/* --------------------------------------------------------- */


private:
	void write8(l3gd20Registers_t reg, uint8_t value);

	uint8_t read8(l3gd20Registers_t reg);


	//  Some private variables
	l3gd20Range_t 			_range;
	l3gd20_bandwidth_t		_bandwidth;
	l3gd20_data_rate_t 		_dataRate;
	l3gd20FifoModes_t		_fifoMode;
	uint8_t 				_fifoDepth;
	bool					_fifoEnabled;
	bool					_fifoStopOnThreshold;


	int32_t 				_sensorID;
	bool 					_autoRangeEnabled;
};

#endif

