// 
// 
// 

#include "HAL_IMU_ADAFRUIT10DOF_Class.h"

//#include "HAL.h"

using namespace AP_HAL;

static L3GD20 l3gd20(HalGyro::gyroDataRates::GYRO_RATE_100HZ,
					 HalGyro::gyroRanges::GYRO_RANGE_250DPS,
					 USE_FIFO,
					 BUFFER_DEPTH,
					 HalGyro::gyroIntTypes::GYRO_HARDWARE_INT);

static BMP180 bmp180; 

HAL_IMU_ADAFRUIT10DOF::HAL_IMU_ADAFRUIT10DOF() 
	:
	HAL((HALBaro *)&bmp180,
		(HalGyro *)&l3gd20)
{}

const HAL_IMU_ADAFRUIT10DOF IMU_HAL_IMU_ADAFRUIT10DOF;